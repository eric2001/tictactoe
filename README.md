# TicTacToe
![TicTacToe](./screenshot.jpg) 

## Description
Play the game Tic-Tac-Toe against the computer.

**License:** [GPL v.3](http://www.gnu.org/licenses/gpl-3.0.html)

## Instructions
Click on an area of the screen to place an X there.  Whoever gets three X's or O's in a row wins.

## History
**Version 2.1.0:**
> - Added an Icon.
> - The program will now randomly determine who will go first.
> - Released 04 March 2009.
>
> Download: [Version 2.1.0 Setup](/uploads/03cfdb34b30d117750eaaf6cf7eb0d5f/TicTacToe2.1.0Setup.zip) | [Version 2.1.0 Source](/uploads/f5ea80c5e3910336b2104cc05d8ab58d/TicTacToe2.1.0Source.zip)

**Version 2.0.0:**
> - Updated Setup program and about screen to reference version 3 of the GPL.
> - The computer AI is actually semi-intelligent now.
> - Released 04 January 2009.
>
> Download: [Version 2.0.0 Setup](/uploads/379c7255b21d3dce36e3953d5a09ec6c/TicTacToe2.0.0Setup.zip) | [Version 2.0.0 Source](/uploads/4b4513f06097b935c3bce5e587e389bf/TicTacToe2.0.0Source.zip)

**Version 1.1.0:**
> - Released source code, now licensed under the GPL Version 3
> - Other minor changes
> - Released 10 October 2007
>
> Download: [Version 1.1.0 Setup](/uploads/9378ca36590fcce414b37849425433fc/TicTacToe1.1.0Setup.zip) | [Version 1.1.0 Source](/uploads/67f72e93066ff0050416aec05be7d587/TicTacToe1.1.0Source.zip)

**Version 1.0.1:**
> - Minor Bugfix
> - Released 27 March 2006
>
> Download: [Version 1.0.1 Setup](/uploads/de7b8ee894dbc8a0f485258b3f0140b2/TicTacToe1.0.1Setup.zip)

**Version 1.0:**
> - Initial Release
> - Released on 23 March 2006
