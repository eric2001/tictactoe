' Copyright 2007-2008 Eric Cavaliere
' Project:     Tic-Tac-Toe
' Start Date:  23 March 2006
' Last Update: 04 March 2009
' License:     GPL Version 3
' Description: Play the game Tic-Tac-Toe against the computer.

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class formBoard
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents pictureO As System.Windows.Forms.PictureBox
    Friend WithEvents pictureX As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents emptyPanel As System.Windows.Forms.Panel
    Friend WithEvents menuNewGame As System.Windows.Forms.MenuItem
    Friend WithEvents menuExit As System.Windows.Forms.MenuItem
    Friend WithEvents menuAbout As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(formBoard))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.pictureO = New System.Windows.Forms.PictureBox
        Me.pictureX = New System.Windows.Forms.PictureBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.Panel9 = New System.Windows.Forms.Panel
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.menuNewGame = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.menuExit = New System.Windows.Forms.MenuItem
        Me.menuAbout = New System.Windows.Forms.MenuItem
        Me.emptyPanel = New System.Windows.Forms.Panel
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(128, 128)
        Me.Panel1.TabIndex = 3
        Me.Panel1.Tag = "1"
        '
        'pictureO
        '
        Me.pictureO.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.pictureO.Image = CType(resources.GetObject("pictureO.Image"), System.Drawing.Image)
        Me.pictureO.Location = New System.Drawing.Point(24, 160)
        Me.pictureO.Name = "pictureO"
        Me.pictureO.Size = New System.Drawing.Size(96, 80)
        Me.pictureO.TabIndex = 5
        Me.pictureO.TabStop = False
        Me.pictureO.Visible = False
        '
        'pictureX
        '
        Me.pictureX.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.pictureX.Image = CType(resources.GetObject("pictureX.Image"), System.Drawing.Image)
        Me.pictureX.Location = New System.Drawing.Point(16, 288)
        Me.pictureX.Name = "pictureX"
        Me.pictureX.Size = New System.Drawing.Size(80, 56)
        Me.pictureX.TabIndex = 6
        Me.pictureX.TabStop = False
        Me.pictureX.Visible = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel2.Location = New System.Drawing.Point(136, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(128, 128)
        Me.Panel2.TabIndex = 7
        Me.Panel2.Tag = "2"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel3.Location = New System.Drawing.Point(272, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(128, 128)
        Me.Panel3.TabIndex = 8
        Me.Panel3.Tag = "3"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel4.Location = New System.Drawing.Point(0, 136)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(128, 128)
        Me.Panel4.TabIndex = 9
        Me.Panel4.Tag = "4"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel5.Location = New System.Drawing.Point(136, 136)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(128, 128)
        Me.Panel5.TabIndex = 10
        Me.Panel5.Tag = "5"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel6.Location = New System.Drawing.Point(272, 136)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(128, 128)
        Me.Panel6.TabIndex = 11
        Me.Panel6.Tag = "6"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel7.Location = New System.Drawing.Point(0, 272)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(128, 128)
        Me.Panel7.TabIndex = 12
        Me.Panel7.Tag = "7"
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel8.Location = New System.Drawing.Point(136, 272)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(128, 128)
        Me.Panel8.TabIndex = 13
        Me.Panel8.Tag = "8"
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Panel9.Location = New System.Drawing.Point(272, 272)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(128, 128)
        Me.Panel9.TabIndex = 14
        Me.Panel9.Tag = "9"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.menuAbout})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuNewGame, Me.MenuItem3, Me.menuExit})
        Me.MenuItem1.Text = "&File"
        '
        'menuNewGame
        '
        Me.menuNewGame.Index = 0
        Me.menuNewGame.Shortcut = System.Windows.Forms.Shortcut.CtrlN
        Me.menuNewGame.Text = "&New Game"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'menuExit
        '
        Me.menuExit.Index = 2
        Me.menuExit.Shortcut = System.Windows.Forms.Shortcut.CtrlQ
        Me.menuExit.Text = "E&xit"
        '
        'menuAbout
        '
        Me.menuAbout.Index = 1
        Me.menuAbout.Text = "&About"
        '
        'emptyPanel
        '
        Me.emptyPanel.Location = New System.Drawing.Point(160, 128)
        Me.emptyPanel.Name = "emptyPanel"
        Me.emptyPanel.Size = New System.Drawing.Size(40, 24)
        Me.emptyPanel.TabIndex = 15
        Me.emptyPanel.Visible = False
        '
        'formBoard
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(392, 377)
        Me.Controls.Add(Me.emptyPanel)
        Me.Controls.Add(Me.Panel9)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.pictureX)
        Me.Controls.Add(Me.pictureO)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Menu = Me.MainMenu1
        Me.Name = "formBoard"
        Me.Opacity = 0.85
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tic-Tac-Toe"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim ticTacToeGrid(3, 3) As Char
    Dim charPlayerPiece As Char
    Dim imgPlayerPiece As System.Windows.Forms.PictureBox
    Dim charComputerPiece As Char
    Dim imgComputerPiece As System.Windows.Forms.PictureBox

    Private Sub Panel1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel1.Click
        playerTurn(Panel1)
    End Sub
    Private Sub Panel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel2.Click
        playerTurn(Panel2)
    End Sub
    Private Sub Panel3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel3.Click
        playerTurn(Panel3)
    End Sub
    Private Sub Panel4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel4.Click
        playerTurn(Panel4)
    End Sub
    Private Sub Panel5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel5.Click
        playerTurn(Panel5)
    End Sub
    Private Sub Panel6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel6.Click
        playerTurn(Panel6)
    End Sub
    Private Sub Panel7_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel7.Click
        playerTurn(Panel7)
    End Sub
    Private Sub Panel8_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel8.Click
        playerTurn(Panel8)
    End Sub
    Private Sub Panel9_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel9.Click
        playerTurn(Panel9)
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Initialize the board.
        Dim i, j As Integer
        For i = 0 To 2
            For j = 0 To 2
                ticTacToeGrid(i, j) = "-"
            Next j
        Next i

        ' Determine who goes first.
        Dim randomNumber As New Random(Now.Millisecond)
        ' if "1", the computer goes, else the player starts.
        If randomNumber.Next(0, 2) = 1 Then
            charComputerPiece = "X"
            imgComputerPiece = pictureX
            charPlayerPiece = "O"
            imgPlayerPiece = pictureO
            computerTurn()
        Else
            charComputerPiece = "O"
            imgComputerPiece = pictureO
            charPlayerPiece = "X"
            imgPlayerPiece = pictureX
        End If
    End Sub

    Private Function noWinner()
        Dim i, j As Integer
        Dim gameover As Boolean
        gameover = True
        For i = 0 To 2
            For j = 0 To 2
                If ticTacToeGrid(i, j) = "-" Then
                    gameover = False
                End If
            Next j
        Next i
        Return gameover
    End Function

    Private Function checkForWin(ByVal playerChar As Char)
        If ticTacToeGrid(0, 0) = playerChar And ticTacToeGrid(0, 1) = playerChar And ticTacToeGrid(0, 2) = playerChar Then
            Return True
        ElseIf ticTacToeGrid(1, 0) = playerChar And ticTacToeGrid(1, 1) = playerChar And ticTacToeGrid(1, 2) = playerChar Then
            Return True
        ElseIf ticTacToeGrid(2, 0) = playerChar And ticTacToeGrid(2, 1) = playerChar And ticTacToeGrid(2, 2) = playerChar Then
            Return True
        ElseIf ticTacToeGrid(0, 0) = playerChar And ticTacToeGrid(1, 0) = playerChar And ticTacToeGrid(2, 0) = playerChar Then
            Return True
        ElseIf ticTacToeGrid(0, 1) = playerChar And ticTacToeGrid(1, 1) = playerChar And ticTacToeGrid(2, 1) = playerChar Then
            Return True
        ElseIf ticTacToeGrid(0, 2) = playerChar And ticTacToeGrid(1, 2) = playerChar And ticTacToeGrid(2, 2) = playerChar Then
            Return True
        ElseIf ticTacToeGrid(0, 0) = playerChar And ticTacToeGrid(1, 1) = playerChar And ticTacToeGrid(2, 2) = playerChar Then
            Return True
        ElseIf ticTacToeGrid(0, 2) = playerChar And ticTacToeGrid(1, 1) = playerChar And ticTacToeGrid(2, 0) = playerChar Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub computerTurn()
        Dim computer As New classComputer(ticTacToeGrid, charComputerPiece, charPlayerPiece)
        Dim computerMove As Integer = 0
        Dim panelCounter As Integer = 0

        ' Determine which pannel ID to take.
        If computer.CheckForComputerWin() > 0 Then
            computerMove = computer.CheckForComputerWin()
        ElseIf computer.CheckForPlayerWin() > 0 Then
            computerMove = computer.CheckForPlayerWin()
        Else
            computerMove = computer.PickSpace()
        End If

        ' Convert Pannel ID to grid #'s, take space.
        Dim i, j As Integer
        For i = 0 To 2
            For j = 0 To 2
                panelCounter = panelCounter + 1
                If panelCounter = computerMove Then
                    ticTacToeGrid(i, j) = charComputerPiece
                    If computerMove = 1 Then
                        Panel1.BackgroundImage = imgComputerPiece.Image
                    End If
                    If computerMove = 2 Then
                        Panel2.BackgroundImage = imgComputerPiece.Image
                    End If
                    If computerMove = 3 Then
                        Panel3.BackgroundImage = imgComputerPiece.Image
                    End If
                    If computerMove = 4 Then
                        Panel4.BackgroundImage = imgComputerPiece.Image
                    End If
                    If computerMove = 5 Then
                        Panel5.BackgroundImage = imgComputerPiece.Image
                    End If
                    If computerMove = 6 Then
                        Panel6.BackgroundImage = imgComputerPiece.Image
                    End If
                    If computerMove = 7 Then
                        Panel7.BackgroundImage = imgComputerPiece.Image
                    End If
                    If computerMove = 8 Then
                        Panel8.BackgroundImage = imgComputerPiece.Image
                    End If
                    If computerMove = 9 Then
                        Panel9.BackgroundImage = imgComputerPiece.Image
                    End If
                End If
            Next j
        Next i

        ' Check and see if the computer just won
        If checkForWin(charComputerPiece) Then
            If MessageBox.Show("You Lose.  Play Again?", "Game Over", MessageBoxButtons.YesNo) = 6 Then
                startNewGame()
            End If
        End If

        ' check and see if the game is a tie.
        If noWinner() Then
            If MessageBox.Show("The game is a tie.  Play Again?", "Game Over", MessageBoxButtons.YesNo) = 6 Then
                startNewGame()
            End If
        End If

    End Sub

    Private Sub playerTurn(ByRef temppanel As Panel)
        Dim PosL, PosT As Integer
        If temppanel.Tag > 6 Then
            PosL = 2
        ElseIf temppanel.Tag > 3 Then
            PosL = 1
        Else
            PosL = 0
        End If

        If temppanel.Tag > 6 Then
            PosT = temppanel.Tag - 7
        ElseIf temppanel.Tag > 3 Then
            PosT = temppanel.Tag - 4
        Else
            PosT = temppanel.Tag - 1
        End If

        If ticTacToeGrid(PosL.ToString, PosT.ToString) = "-" Then
            ticTacToeGrid(PosL.ToString, PosT.ToString) = charPlayerPiece
            temppanel.BackgroundImage = imgPlayerPiece.Image
            If checkForWin("X") Then
                If MessageBox.Show("You Win!  Play Again?", "Game Over", MessageBoxButtons.YesNo) = 6 Then
                    startNewGame()
                End If
            ElseIf noWinner() Then
                If MessageBox.Show("The game is a tie.  Play Again?", "Game Over", MessageBoxButtons.YesNo) = 6 Then
                    startNewGame()
                End If
            Else
                computerTurn()
            End If
        Else
            MessageBox.Show("Try Again", "Error")
        End If

    End Sub

    Private Sub menuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuExit.Click
        Application.Exit()
    End Sub

    Private Sub menuNewGame_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuNewGame.Click
        startNewGame()
    End Sub

    Private Sub startNewGame()
        ' Reset the board.
        Dim i, j As Integer
        For i = 0 To 2
            For j = 0 To 2
                ticTacToeGrid(i, j) = "-"
            Next j
        Next i
        Panel1.BackgroundImage = emptyPanel.BackgroundImage
        Panel2.BackgroundImage = emptyPanel.BackgroundImage
        Panel3.BackgroundImage = emptyPanel.BackgroundImage
        Panel4.BackgroundImage = emptyPanel.BackgroundImage
        Panel5.BackgroundImage = emptyPanel.BackgroundImage
        Panel6.BackgroundImage = emptyPanel.BackgroundImage
        Panel7.BackgroundImage = emptyPanel.BackgroundImage
        Panel8.BackgroundImage = emptyPanel.BackgroundImage
        Panel9.BackgroundImage = emptyPanel.BackgroundImage

        ' Determine who goes first.
        Dim randomNumber As New Random(Now.Millisecond)
        Dim winner As Integer = randomNumber.Next(0, 2)
        Console.WriteLine(winner)
        ' if "1", the computer goes, else the player starts.
        If winner = 1 Then
            charComputerPiece = "X"
            imgComputerPiece = pictureX
            charPlayerPiece = "O"
            imgPlayerPiece = pictureO
            computerTurn()
        Else
            charComputerPiece = "O"
            imgComputerPiece = pictureO
            charPlayerPiece = "X"
            imgPlayerPiece = pictureX
        End If

    End Sub

    Private Sub menuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuAbout.Click
        Dim aboutWindow As New about
        aboutWindow.Show()
    End Sub
End Class
