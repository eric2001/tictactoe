' Copyright 2007-2008 Eric Cavaliere
' Project:     Tic-Tac-Toe
' Start Date:  23 March 2006
' License:     GPL Version 3
' Description: Play the game Tic-Tac-Toe against the computer.

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class about
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lblCopyright As System.Windows.Forms.Label
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents buttonOK As System.Windows.Forms.Button
    Friend WithEvents lblLicense1 As System.Windows.Forms.Label
    Friend WithEvents lblLicense2 As System.Windows.Forms.Label
    Friend WithEvents lblLicense3 As System.Windows.Forms.Label
    Friend WithEvents pictureTicTacToe As System.Windows.Forms.PictureBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(about))
        Me.lblTitle = New System.Windows.Forms.Label
        Me.lblCopyright = New System.Windows.Forms.Label
        Me.lblVersion = New System.Windows.Forms.Label
        Me.buttonOK = New System.Windows.Forms.Button
        Me.lblLicense1 = New System.Windows.Forms.Label
        Me.lblLicense2 = New System.Windows.Forms.Label
        Me.lblLicense3 = New System.Windows.Forms.Label
        Me.pictureTicTacToe = New System.Windows.Forms.PictureBox
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(108, 8)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(139, 31)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "Tic Tac Toe"
        '
        'lblCopyright
        '
        Me.lblCopyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCopyright.Location = New System.Drawing.Point(1, 72)
        Me.lblCopyright.Name = "lblCopyright"
        Me.lblCopyright.Size = New System.Drawing.Size(352, 32)
        Me.lblCopyright.TabIndex = 1
        Me.lblCopyright.Text = "Copyright 2006-2009 Eric Cavaliere"
        Me.lblCopyright.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVersion
        '
        Me.lblVersion.AutoSize = True
        Me.lblVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.Location = New System.Drawing.Point(104, 40)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(147, 25)
        Me.lblVersion.TabIndex = 2
        Me.lblVersion.Text = "Version X.X.X.X"
        '
        'buttonOK
        '
        Me.buttonOK.Location = New System.Drawing.Point(129, 296)
        Me.buttonOK.Name = "buttonOK"
        Me.buttonOK.Size = New System.Drawing.Size(96, 40)
        Me.buttonOK.TabIndex = 3
        Me.buttonOK.Text = "Okay"
        '
        'lblLicense1
        '
        Me.lblLicense1.Location = New System.Drawing.Point(5, 120)
        Me.lblLicense1.Name = "lblLicense1"
        Me.lblLicense1.Size = New System.Drawing.Size(344, 56)
        Me.lblLicense1.TabIndex = 4
        Me.lblLicense1.Text = "This program is free software: you can redistribute it and/or modify it under the" & _
        " terms of the GNU General Public License as published by the Free Software Found" & _
        "ation, either version 3 of the License, or (at your option) any later version."
        '
        'lblLicense2
        '
        Me.lblLicense2.Location = New System.Drawing.Point(5, 184)
        Me.lblLicense2.Name = "lblLicense2"
        Me.lblLicense2.Size = New System.Drawing.Size(344, 56)
        Me.lblLicense2.TabIndex = 5
        Me.lblLicense2.Text = "This program is distributed in the hope that it will be useful, but WITHOUT ANY W" & _
        "ARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A P" & _
        "ARTICULAR PURPOSE.  See the GNU General Public License for more details."
        '
        'lblLicense3
        '
        Me.lblLicense3.Location = New System.Drawing.Point(5, 248)
        Me.lblLicense3.Name = "lblLicense3"
        Me.lblLicense3.Size = New System.Drawing.Size(344, 40)
        Me.lblLicense3.TabIndex = 6
        Me.lblLicense3.Text = "You should have received a copy of the GNU General Public License along with this" & _
        " program.  If not, see <http://www.gnu.org/licenses/>."
        '
        'pictureTicTacToe
        '
        Me.pictureTicTacToe.Image = CType(resources.GetObject("pictureTicTacToe.Image"), System.Drawing.Image)
        Me.pictureTicTacToe.Location = New System.Drawing.Point(40, 16)
        Me.pictureTicTacToe.Name = "pictureTicTacToe"
        Me.pictureTicTacToe.Size = New System.Drawing.Size(32, 32)
        Me.pictureTicTacToe.TabIndex = 7
        Me.pictureTicTacToe.TabStop = False
        '
        'about
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(354, 344)
        Me.Controls.Add(Me.pictureTicTacToe)
        Me.Controls.Add(Me.lblLicense3)
        Me.Controls.Add(Me.lblLicense2)
        Me.Controls.Add(Me.lblLicense1)
        Me.Controls.Add(Me.buttonOK)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.lblCopyright)
        Me.Controls.Add(Me.lblTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "about"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "About"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub buttonOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonOK.Click
        Me.Close()
    End Sub

    Private Sub about_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblVersion.Text = "Version " + Application.ProductVersion
    End Sub
End Class
