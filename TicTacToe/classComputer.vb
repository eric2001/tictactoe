' Copyright 2007-2008 Eric Cavaliere
' Project:     Tic-Tac-Toe
' Start Date:  23 March 2006
' License:     GPL Version 3
' Description: Play the game Tic-Tac-Toe against the computer.

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class classComputer
    Dim gameBoard(3, 3) As Char
    Dim playerGamePiece As Char
    Dim computerGamePiece As Char

    Sub New(ByRef board(,) As Char, ByVal computerSym As Char, ByVal playerSym As Char)
        ' This sub takes three arguments, the game board (board), and an X or O to represent the 
        '   computer and the player.
        gameBoard = board
        playerGamePiece = playerSym
        computerGamePiece = computerSym
    End Sub

    Public Function CheckForComputerWin() As Integer
        ' Check and see if the computer is one move away from winning.
        '   If so, return the pannel id (1 to 9), if not return 0.
        Dim spacesInARow As Integer = 0

        Dim colCounter As Integer
        Dim rowCounter As Integer

        ' Check for Win's by row.
        For rowCounter = 0 To 2
            spacesInARow = 0
            For colCounter = 0 To 2
                If gameBoard(rowCounter, colCounter) = computerGamePiece Then
                    spacesInARow = spacesInARow + 1
                End If
            Next
            If spacesInARow = 2 Then
                Dim emptySpace As Integer = -1
                For colCounter = 0 To 2
                    If gameBoard(rowCounter, colCounter) <> computerGamePiece Then
                        If gameBoard(rowCounter, colCounter) = "-" Then
                            emptySpace = colCounter
                        End If
                    End If
                Next
                If emptySpace <> -1 Then
                    emptySpace = (emptySpace + 1) + (rowCounter * 3)
                    Return emptySpace
                End If
            End If
        Next

        ' Check for Wins by Column
        For colCounter = 0 To 2
            spacesInARow = 0
            For rowCounter = 0 To 2
                If gameBoard(rowCounter, colCounter) = computerGamePiece Then
                    spacesInARow = spacesInARow + 1
                End If
            Next
            If spacesInARow = 2 Then
                Dim emptySpace As Integer = -1
                For rowCounter = 0 To 2
                    If gameBoard(rowCounter, colCounter) <> computerGamePiece Then
                        If gameBoard(rowCounter, colCounter) = "-" Then
                            emptySpace = rowCounter
                        End If
                    End If
                Next
                If emptySpace <> -1 Then
                    emptySpace = (emptyspace * 3) + (colCounter + 1)
                    Return emptySpace
                End If
            End If
        Next

        ' Check for diagonal Wins
        spacesInARow = 0
        If gameBoard(0, 0) = computerGamePiece Then
            spacesInARow = spacesInARow + 1
        End If
        If gameBoard(1, 1) = computerGamePiece Then
            spacesInARow = spacesInARow + 1
        End If
        If gameBoard(2, 2) = computerGamePiece Then
            spacesInARow = spacesInARow + 1
        End If
        If spacesInARow = 2 Then
            If gameBoard(0, 0) = "-" Then
                Return 1
            End If
            If gameBoard(1, 1) = "-" Then
                Return 5
            End If
            If gameBoard(2, 2) = "-" Then
                Return 9
            End If
        End If
        spacesInARow = 0
        If gameBoard(2, 0) = computerGamePiece Then
            spacesInARow = spacesInARow + 1
        End If
        If gameBoard(1, 1) = computerGamePiece Then
            spacesInARow = spacesInARow + 1
        End If
        If gameBoard(0, 2) = computerGamePiece Then
            spacesInARow = spacesInARow + 1
        End If
        If spacesInARow = 2 Then
            If gameBoard(2, 0) = "-" Then
                Return 7
            End If
            If gameBoard(1, 1) = "-" Then
                Return 5
            End If
            If gameBoard(0, 2) = "-" Then
                Return 3
            End If
        End If

        Return 0
    End Function

    Public Function CheckForPlayerWin() As Integer
        ' Check and see if the player is one move away from winning.
        '   If so, return the pannel id (1 to 9), if not return 0.
        Dim spacesInARow As Integer = 0

        Dim colCounter As Integer
        Dim rowCounter As Integer

        ' Check for Wins by row.
        For rowCounter = 0 To 2
            spacesInARow = 0
            For colCounter = 0 To 2
                If gameBoard(rowCounter, colCounter) = playerGamePiece Then
                    spacesInARow = spacesInARow + 1
                End If
            Next
            If spacesInARow = 2 Then
                Dim emptySpace As Integer = -1
                For colCounter = 0 To 2
                    If gameBoard(rowCounter, colCounter) <> playerGamePiece Then
                        If gameBoard(rowCounter, colCounter) = "-" Then
                            emptySpace = colCounter
                        End If
                    End If
                Next
                If emptySpace <> -1 Then
                    emptySpace = (emptySpace + 1) + (rowCounter * 3)
                    Return emptySpace
                End If
            End If
        Next

        ' Check for Wins by Column
        For colCounter = 0 To 2
            spacesInARow = 0
            For rowCounter = 0 To 2
                If gameBoard(rowCounter, colCounter) = playerGamePiece Then
                    spacesInARow = spacesInARow + 1
                End If
            Next
            If spacesInARow = 2 Then
                Dim emptySpace As Integer = -1
                For rowCounter = 0 To 2
                    If gameBoard(rowCounter, colCounter) <> playerGamePiece Then
                        If gameBoard(rowCounter, colCounter) = "-" Then
                            emptySpace = rowCounter
                        End If
                    End If
                Next
                If emptySpace <> -1 Then
                    emptySpace = (emptyspace * 3) + (colCounter + 1)
                    Return emptySpace
                End If
            End If
        Next

        ' Check for diagonal Wins
        spacesInARow = 0
        If gameBoard(0, 0) = playerGamePiece Then
            spacesInARow = spacesInARow + 1
        End If
        If gameBoard(1, 1) = playerGamePiece Then
            spacesInARow = spacesInARow + 1
        End If
        If gameBoard(2, 2) = playerGamePiece Then
            spacesInARow = spacesInARow + 1
        End If
        If spacesInARow = 2 Then
            If gameBoard(0, 0) = "-" Then
                Return 1
            End If
            If gameBoard(1, 1) = "-" Then
                Return 5
            End If
            If gameBoard(2, 2) = "-" Then
                Return 9
            End If
        End If
        spacesInARow = 0
        If gameBoard(2, 0) = playerGamePiece Then
            spacesInARow = spacesInARow + 1
        End If
        If gameBoard(1, 1) = playerGamePiece Then
            spacesInARow = spacesInARow + 1
        End If
        If gameBoard(0, 2) = playerGamePiece Then
            spacesInARow = spacesInARow + 1
        End If
        If spacesInARow = 2 Then
            If gameBoard(2, 0) = "-" Then
                Return 7
            End If
            If gameBoard(1, 1) = "-" Then
                Return 5
            End If
            If gameBoard(0, 2) = "-" Then
                Return 3
            End If
        End If

        Return 0
    End Function

    Public Function PickSpace() As Integer
        ' Pick an empty space for the computer's turn.
        '  returns the chosen pannel number.

        Dim randomSpace As Integer
        Dim randomNumber As New Random(Now.Millisecond)


        ' If the middle square is empty, take it.
        If gameBoard(1, 1) = "-" Then
            Return 5
        End If

        'if any of the corner spaces are empty, take one
        Dim emptyCorners As Integer = 0
        If gameBoard(0, 0) = "-" Then
            emptyCorners = emptyCorners + 1
        End If
        If gameBoard(0, 2) = "-" Then
            emptyCorners = emptyCorners + 1
        End If
        If gameBoard(2, 0) = "-" Then
            emptyCorners = emptyCorners + 1
        End If
        If gameBoard(2, 2) = "-" Then
            emptyCorners = emptyCorners + 1
        End If
        If emptyCorners > 0 Then
            randomSpace = randomNumber.Next(1, emptyCorners)
            'Randomize()
            'randomSpace = Int((Rnd() * emptyCorners)) + 1
            Dim CornerCounter As Integer = 0
            If gameBoard(0, 0) = "-" Then
                CornerCounter = CornerCounter + 1
                If CornerCounter = randomSpace Then
                    Return 1
                End If
            End If
            If gameBoard(0, 2) = "-" Then
                CornerCounter = CornerCounter + 1
                If CornerCounter = randomSpace Then
                    Return 3
                End If
            End If
            If gameBoard(2, 0) = "-" Then
                CornerCounter = CornerCounter + 1
                If CornerCounter = randomSpace Then
                    Return 7
                End If
            End If
            If gameBoard(2, 2) = "-" Then
                CornerCounter = CornerCounter + 1
                If CornerCounter = randomSpace Then
                    Return 9
                End If
            End If
        End If

        ' Take a random open space.
        ' Determine how many space are empty
        'Randomize()
        Dim emptySpaces, spacesCounter, panelCounter As Integer
        emptySpaces = 0
        panelCounter = 0
        spacesCounter = 0
        Dim i, j As Integer
        For i = 0 To 2
            For j = 0 To 2
                If gameBoard(i, j) = "-" Then
                    emptySpaces = emptySpaces + 1
                End If
            Next j
        Next i

        ' Randomly select one of the empty spaces.
        ' randomSpace is max valued 0 though 8 depending on the number of empty spaces
        randomSpace = randomNumber.Next(1, emptySpaces)
        'randomSpace = Int((Rnd() * emptySpaces)) + 1
        For i = 0 To 2
            For j = 0 To 2
                panelCounter = panelCounter + 1
                If gameBoard(i, j) = "-" Then
                    spacesCounter = spacesCounter + 1
                    If spacesCounter = randomSpace Then
                        Return panelCounter
                    End If
                End If
            Next j
        Next i

        'if there are no spaces left.
        Return 0
    End Function
End Class
